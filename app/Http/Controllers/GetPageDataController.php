<?php

namespace App\Http\Controllers;

use Goutte\Client;
use GuzzleHttp\Client as GuzzleClient;

class GetPageDataController extends Controller
{
    /**
     * getting data from provided url
     *
     * @param string $url
     * @return \Illuminate\Http\JsonResponse
     */
    public function getData($url = 'https://www.amazon.co.uk/Winning-Moves-29612-Trivial-Pursuit/dp/B075716WLM/')
    {
        $url = request('url') ? request('url') : $url;
        $goutteClient = new Client();
        $guzzleClient = new GuzzleClient(array(
            'timeout' => 60,
        ));
        $goutteClient->setClient($guzzleClient);
        $crawler = $goutteClient->request('GET', $url);

        // the selectors which are used
        $cssSelectors = [
            'title'        => 'title',
            'asin'         => '.col2 .attrG tbody tr:first-child > td:nth-child(2)',
            'images'       => '#altImages li:not(.aok-hidden) span img',
            'price'        => '#olp_feature_div .a-color-price',
            'description'  => '#productDescription p:nth-child(2)',
            'product_spec' => '.col1 .attrG tbody tr td'
        ];

        $response = [];
        foreach ($cssSelectors as $key => $selector) {
            if ($key == 'images') {
                $output = $crawler->filter($selector)->extract(array('src'));
            } else {
                $output = $crawler->filter($selector)->extract(array('_text'));
            }
            foreach ($output as $key1 => $items) {
                if (! str_replace('&nbsp;', '', htmlentities($items))) {
                    unset($output[$key1]);
                }
            }

            if (count($output) == 1 && $key != 'product_spec') {
                $response[$key] = $output[0];
            } elseif ($key != 'product_spec') {
                $response[$key] = $output;
            }

            // For specification
            // as we have key value pairs, dividing them into
            // odd and even items, then combining them together.
            if ($key == 'product_spec') {
                $oddItems  = [];
                $evenItems = [];
                $bothItems = array(&$evenItems, &$oddItems);
                array_walk($output, function ($v, $k) use ($bothItems) {
                    $bothItems[$k % 2][] = $v;
                });
                $bothItems      = array_combine($evenItems, $oddItems);
                $response[$key] = $bothItems;
            }
        }

        return response()->json($response);
    }
}
